<?php 
	// $_GET and $_POST are "super global variables" in PHP.
	// Allow you to retrieve information sent by the client.

	//"super global" variables allow data to persists between pages or a single session.

	//Both $_GET and $_POST creates an associative array that holds key=>value pair.
		// "key" represents name of the form control element.
		// "value" represents the inputted data from the user.

	//var_dump($_GET);
	//var_dump($_POST);


	$tasks = ["Get Git", "Bake HTML", "Eat CSS", "Learn PHP"];

	// isset() function Checks if the variable is set.
	//true if a variable/key is set, false if the return is NULL.
	if(isset($_GET["index"])){

	$indexGet = $_GET["index"];
	echo "The retrieved task from GET is $tasks[$indexGet] <br>";
	}

	if(isset($_POST["index"])){

	$indexGet = $_POST["index"];
	echo "The retrieved task from POST is $tasks[$indexGet] <br>";
	}

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>S05 - Client-Server Communication (GET and POST)</title>
</head>
<body>
	<h1>Task index from GET</h1>
	<form method="GET">
		<select name="index" required>
			<option value="0">0</option>
			<option value="1">1</option>
			<option value="2">2</option>
			<option value="3">3</option>
		</select>

		<button type="submit">GET</button>

		<!-- <label>Email:</label>
		<input type="email" name="email"><br>
		<label>Password:</label>
		<input type="password" name="password"><br>

		<button type="submit">Login</button> -->
	</form>

	<h1>Task index from POST</h1>
	<form method="POST">
		<select name="index" required>
			<option value="0">0</option>
			<option value="1">1</option>
			<option value="2">2</option>
			<option value="3">3</option>
		</select>
		<button type="submit">POST</button>
	</form>

</body>
</html>